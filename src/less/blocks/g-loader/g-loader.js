$(function () {
    $(window).on('DOMContentLoaded load resize scroll', function () {
        $('.g-loader').each(function () {
            if ($(this).visible(true)) {
                $(this).removeClass('g-loader').addClass('g-loading');
            }
        });
    });
});