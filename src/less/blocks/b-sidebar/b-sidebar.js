$('.b-sidebar__open').on('click', function (e) {
    e.preventDefault();
    $(this).closest('.b-sidebar').addClass('b-sidebar_opened');
    $('body').addClass('modal-open');
});

$('.b-sidebar__close').on('click', function (e) {
    e.preventDefault();
    $(this).closest('.b-sidebar').removeClass('b-sidebar_opened');
    $('body').removeClass('modal-open');
});