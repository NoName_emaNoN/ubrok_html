var sectionHeight = 684;
var skrollrMultiplier = 4;
var t = 0;
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); // viewport height
var lastSectionHeight = 612;
var section12Height = 980;
var anchors = [0];
var slugs = window.slugs || ['#section-1', '#section-2', '#section-3', '#section-4', '#section-5', '#section-6', '#section-7', '#section-8', '#section-9', '#section-10', '#section-11', '#section-12', '#section-14', '#section-15', '#section-13'];
var additionalSectionsHeight = 0;
var s;

$(document).on('ready', function () {
    setTimeout(function () {

        var durationValueFunction = function (currentTop, targetTop) {
            //But you could calculate a value based on the current scroll position (`currentTop`) and the target scroll position (`targetTop`).
            return Math.min(Math.abs(currentTop - targetTop) * 0.15, 600);
        };

        function checkNavigation(element, name, direction) {
            if (typeof s === "undefined")
                return;

            console.log('keyframe', element, name, direction);

            $('.b-sidebar__anchors-item_active').removeClass('b-sidebar__anchors-item_active');
            var currentTop = s.getScrollTop();

            console.log('check navigation', {currentTop: currentTop, anchors: anchors});
            for (var i in anchors) {
                i = parseInt(i);

                if (i < anchors.length - 1) {
                    if (currentTop >= anchors[i] && currentTop < anchors[i + 1]) {
                        $('a[href="' + slugs[i] + '"]').parent().addClass('b-sidebar__anchors-item_active');

                        window.history.replaceState({}, '', slugs[i]);
                        console.log('Target is #section-' + (parseInt(i) + 1).toString(), {currentTop: currentTop, index: i});
                        break;
                    }
                }
                if (i == anchors.length - 1) {
                    if (currentTop >= anchors[anchors.length - 1]) {
                        $('a[href="' + slugs[i] + '"]').parent().addClass('b-sidebar__anchors-item_active');
                        window.history.replaceState({}, '', slugs[i]);
                        console.log('Target is #section-' + (parseInt(i) + 1).toString());
                        //location.hash = '#section-' + (parseInt(i) + 1).toString();
                    }
                }
            }
        }

        if (!(/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
            $('body').removeClass('skrollr-mobile');

            var j = 0;

            $('.b-section_additional').each(function () {
                additionalSectionsHeight += $(this).height();
            }).each(function () {
                //$(this).attr('data-_frame86', 'top:' + section12Height + 'px;transform:translateY(' + j + 'px);')
                //    .attr('data-_frame87', 'top:' + section12Height + 'px;transform:translateY(-' + (additionalSectionsHeight - j + lastSectionHeight - (h - section12Height)) + 'px);');
                $(this).attr('data-_frame86', 'top:' + section12Height + 'px;transform:translateY(' + j + 'px);')
                    .attr('data-_frame87', 'top:' + section12Height + 'px;transform:translateY(-' + (additionalSectionsHeight - j + lastSectionHeight - (h - section12Height)) + 'px);');

                console.log('height of additional section: ' + $(this).height());
                j += $(this).height();
            });

            $('.b-section-12').attr('data-_frame87', 'top:0px;transform:translateY(-' + (additionalSectionsHeight + lastSectionHeight + section12Height - h) + 'px);');
            $('.b-section-13')
                .attr('data-_frame86', 'top:' + section12Height + 'px;transform:translateY(' + additionalSectionsHeight + 'px);')
                .attr('data-_frame87', 'top:' + section12Height + 'px;transform:translateY(' + ((lastSectionHeight + Math.abs(h - section12Height)) * -1) + 'px);');

            var i = 0;

            s = skrollr.init({
                constants: {
                    frame0: 0 * skrollrMultiplier,
                    frame1: anchors[1] = i += ((sectionHeight) * skrollrMultiplier), // section2.start
                    frame2: i += ((10) * skrollrMultiplier), // section2.slide1.balloon.show.start
                    frame3: i += ((300) * skrollrMultiplier), // section2.slide1.balloon.show.end
                    frame4: i += ((200) * skrollrMultiplier), // section2.slide1.balloon.hide.start
                    frame5: i += ((200) * skrollrMultiplier), // section2.slide1.balloon.hide.end
                    frame6: i += ((200) * skrollrMultiplier), // section2.slide2.balloon.show.start
                    frame7: i += ((200) * skrollrMultiplier), // section2.slide2.balloon.show.end
                    frame8: i += ((100) * skrollrMultiplier), // section2.slide2.show.end

                    frame9: i += ((200) * skrollrMultiplier), // section3.show.start
                    frame10: i += ((80) * skrollrMultiplier), // section3.show.start.step2
                    frame11: i += ((100) * skrollrMultiplier), // section2.slide2.balloon.hide.start
                    frame12: i += ((300) * skrollrMultiplier), // section2.slide2.balloon.hide.end
                    frame13: i += ((300) * skrollrMultiplier), // section3.balloon.show.start
                    frame14: i += ((300) * skrollrMultiplier), // section3.balloon.show.end
                    frame15: anchors[2] = i += ((300) * skrollrMultiplier), // section3.show.end

                    frame16: i += ((50) * skrollrMultiplier), // section4.show.start
                    frame17: anchors[3] = i += ((sectionHeight) * skrollrMultiplier), // section4.show.end
                    frame18: i += ((50) * skrollrMultiplier), // section4.car.show.start
                    frame19: i += ((400) * skrollrMultiplier), // section4.car.show.end
                    frame20: t = i += ((0) * skrollrMultiplier), // section4.background.move.start
                    frame21: i += ((100) * skrollrMultiplier), // section4.balloon.show.start
                    frame22: i += ((200) * skrollrMultiplier), // section4.balloon.show.end
                    frame23: i += ((30) * skrollrMultiplier), // section4.car.roof.show.start
                    frame24: i += ((200) * skrollrMultiplier), // section4.car.roof.show.end
                    frame25: i += ((120) * skrollrMultiplier), // section4.car.roof.hide.start
                    frame26: i += ((300) * skrollrMultiplier), // section4.car.roof.hide.end
                    frame27: i += ((100) * skrollrMultiplier), // section4.balloon.hide.start
                    frame28: i += ((150) * skrollrMultiplier), // section4.balloon.hide.end
                    frame29: i = t + ((2244 - h) * skrollrMultiplier), // section4.hide.start, section5.show.start
                    frame30: anchors[4] = i += ((h) * skrollrMultiplier), // section4.hide.end, section5.show.end
                    ///-------
                    frame31: t = i += ((0) * skrollrMultiplier), // section5.background.move.start, section5.plane.stage1.start
                    frame32: i += ((280) * skrollrMultiplier), // section5.plane.stage1.end
                    frame33: i += ((0) * skrollrMultiplier), // section5.plane.stage2.start
                    frame34: i += ((150) * skrollrMultiplier), // section5.balloon.show.start
                    frame35: i += ((200) * skrollrMultiplier), // section5.balloon.show.end
                    frame36: i += ((150) * skrollrMultiplier), // section5.plane.stage2.end
                    frame37: i += ((450) * skrollrMultiplier), // section5.balloon.hide.start
                    frame38: i += ((450) * skrollrMultiplier), // section5.balloon.hide.end

                    frame39: i = t + ((2420 - h) * skrollrMultiplier), // section5.hide.start, section6.show.start
                    frame40: i += ((h - 80) * skrollrMultiplier), // section5.hide.on
                    frame41: anchors[5] = i += ((80) * skrollrMultiplier), // section5.hide.end, section6.show.end
                    ///------
                    frame42: t = i += ((0) * skrollrMultiplier), // section6.background.move.start, section6.ship.stage1.start
                    frame43: i += ((300) * skrollrMultiplier), // section6.balloon.show.start
                    frame44: i += ((100) * skrollrMultiplier), // section6.balloon.show.end
                    frame45: i += ((250) * skrollrMultiplier), // section6.balloon.hide.start
                    frame46: i += ((150) * skrollrMultiplier), // section6.balloon.hide.end
                    frame47: i += ((50) * skrollrMultiplier), // section6.ship.stage1.end
                    frame48: i += ((0) * skrollrMultiplier), // section6.ship.stage2.start

                    frame49: i = t + ((1900 - h) * skrollrMultiplier), // section6.hide.start
                    frame50: i += ((h - 80) * skrollrMultiplier), // section6.hide.on
                    frame51: anchors[6] = i += ((80) * skrollrMultiplier), // section6.hide.end
                    ///------
                    frame52: t = i += ((0) * skrollrMultiplier), // section7.background.move.start, section7.train.stage1.start
                    frame53: i += ((100) * skrollrMultiplier), // section7.balloon.show.start
                    frame54: i += ((100) * skrollrMultiplier), // section7.balloon.show.end
                    frame55: i += ((550) * skrollrMultiplier), // section7.balloon.hide.start
                    frame56: i += ((100) * skrollrMultiplier), // section7.balloon.hide.end
                    frame57: i += ((50) * skrollrMultiplier), // section7.train.stage1.end
                    frame58: i += ((0) * skrollrMultiplier), // section7.train.stage2.start

                    frame59: i = t + ((1900 - h) * skrollrMultiplier), // section7.hide.start
                    frame60: i += ((h - 80) * skrollrMultiplier), // section7.hide.on
                    frame61: i += ((80) * skrollrMultiplier), // section7.hide.end

                    ///------
                    frame62: anchors[7] = t = i += ((0) * skrollrMultiplier), // section8.background.move.start, section8.xray.stage1.start
                    frame63: i += ((150) * skrollrMultiplier), // section8.xray.stage2.start, section8.xray.stage1.end
                    frame64: i += ((100) * skrollrMultiplier), // section8.balloon.show.start
                    frame65: i += ((250) * skrollrMultiplier), // section8.xray.stage2.end
                    frame66: i += ((50) * skrollrMultiplier), // section8.balloon.show.end
                    frame67: i += ((350) * skrollrMultiplier), // section8.xray.stage3.start, section8.background.move.stop
                    frame68: i += ((300) * skrollrMultiplier), // section8.xray.stage3.end
                    frame69: i += ((0) * skrollrMultiplier), // section8.balloon.hide.start
                    frame70: i += ((150) * skrollrMultiplier), // section8.balloon.hide.end
                    frame71: i += ((50) * skrollrMultiplier), // section8.xray.stage4.start
                    frame72: i += ((50) * skrollrMultiplier), // section8.xray.stage4.on
                    frame73: i += ((450) * skrollrMultiplier), // section8.xray.stage4.end, section9.show.start
                    ///-----
                    frame74: anchors[8] = i += ((550) * skrollrMultiplier), // section9.show.end
                    frame75: i += ((0) * skrollrMultiplier), // section9.balloon.show.start, section9.text.move.start
                    frame76: i += ((200) * skrollrMultiplier), // section9.balloon.show.end
                    frame77: t = i += ((400) * skrollrMultiplier), // section9.balloon.hide.start, section9.text.move.end, section10.show.start
                    frame78: i += ((200) * skrollrMultiplier), // section9.balloon.hide.end
                    ///-------
                    frame79: i = t + ((80) * skrollrMultiplier), // section10.show.continue (title)
                    frame80: anchors[9] = i += ((300) * skrollrMultiplier), // section10.show.end
                    frame81: i += ((100) * skrollrMultiplier), // section10.balloon.show.start
                    frame82: i += ((200) * skrollrMultiplier), // section10.balloon.show.end
                    frame83: i += ((400) * skrollrMultiplier), // section10.balloon.hide.start
                    frame84: i += ((300) * skrollrMultiplier), // section10.balloon.hide.end, section11.show.start

                    ///-------
                    frame85: anchors[10] = i += ((400) * skrollrMultiplier), // section11.show.end
                    frame86: anchors[11] = i += ((sectionHeight) * skrollrMultiplier), // continue, top of section-12. now section 12 have top=0px
                    //frame861: i += ((additionalSectionsHeight) * skrollrMultiplier), // continue
                    frame87: anchors[12] = i += ((section12Height + additionalSectionsHeight + lastSectionHeight - h) * skrollrMultiplier), // continue, this value should be equal last block bottom


                    section1: sectionHeight * skrollrMultiplier,
                    section1frame1: (sectionHeight + 700 ) * skrollrMultiplier,
                    section1frame2: (sectionHeight + 1500 ) * skrollrMultiplier,
                    section1frame3: (sectionHeight + 2000 ) * skrollrMultiplier,
                    section1frame4: (sectionHeight + 2200 ) * skrollrMultiplier,
                    section1frame5: (sectionHeight + 2500 ) * skrollrMultiplier,
                    section15: (sectionHeight + 2500 ) * skrollrMultiplier,
                    section16: (sectionHeight + 2600 ) * skrollrMultiplier,
                    section17: (sectionHeight + 2700 ) * skrollrMultiplier,
                    section18: (sectionHeight + 3300 ) * skrollrMultiplier,
                    section2: (sectionHeight * 2) * skrollrMultiplier,
                    section21: (sectionHeight * 2 + 1700) * skrollrMultiplier,
                    section22: (sectionHeight * 2 + 2200) * skrollrMultiplier,
                    section23: (sectionHeight * 2 + 2600) * skrollrMultiplier,
                    section3: (sectionHeight * 3) * skrollrMultiplier,
                    section31: (sectionHeight * 3 + 2600) * skrollrMultiplier,

                    section4: (sectionHeight * 3 + 2244 + 80) * skrollrMultiplier, // 3 секция длиннее остальных
                    section41: (sectionHeight * 3 + 2244 + 80 + 2600) * skrollrMultiplier, // 3 секция длиннее остальных
                    section4pre: function () {
                        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); // viewport height

                        return (sectionHeight * 3 + 2244 + 80 / 2 - h) * skrollrMultiplier;
                    },
                    section41pre: function () {
                        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); // viewport height

                        return (sectionHeight * 3 + 2244 + 80 / 2 + 2600 - h) * skrollrMultiplier;
                    },
                },
                smoothScrolling: false,
                smoothScrollingDuration: 50,
                mobileCheck: function () {
                    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
                },
                //keyframe: checkNavigation
            });

            var k = 11;
            var dk = (anchors[12] - anchors[11]);// / skrollrMultiplier - (h - lastSectionHeight);

            j = 0;

            $('.b-section_additional').each(function () {
                anchors[++k] = anchors[11] + Math.round((h + j) / (additionalSectionsHeight + lastSectionHeight) * dk);
                j += $(this).height();
            });

            anchors[++k] = anchors[11] + dk;

            for (var i in slugs) {
                $('a[href="' + slugs[i] + '"]').attr('data-menu-top', anchors[i]).on('click', function (e) {
                    e.preventDefault();

                    var href = $(this).attr('href');
                    var i = slugs.indexOf(href);

                    console.log(i, slugs[i]);
                    //window.history.replaceState({}, '', slugs[i]);
                    //s.off('render');
                    s.animateTo(anchors[i], {duration: durationValueFunction(s.getScrollTop(), anchors[i])});
                    //return anchors[i];
                });
            }


            skrollr.menu.init(s, {
                //How long the animation should take in ms.
                duration: durationValueFunction,

                handleLink: function (link) {
                    console.log('handle link: ' + link);
                    var href = $(link).attr('href');

                    var i = slugs.indexOf(href);

                    console.log(link, i);
                    //window.history.replaceState({}, '', slugs[i]);
                    return anchors[i];
                },

                //By default skrollr-menu will only react to links whose href attribute contains a hash and nothing more, e.g. `href="#foo"`.
                //If you enable `complexLinks`, skrollr-menu also reacts to absolute and relative URLs which have a hash part.
                //The following will all work (if the user is on the correct page):
                //http://example.com/currentPage/#foo
                //http://example.com/currentDir/currentPage.html?foo=bar#foo
                ///?foo=bar#foo
                complexLinks: true,

                //This event is triggered right before we jump/animate to a new hash.
                change: function (newHash, newTopPosition) {
                    //Do stuff
                    console.log('change', newHash);
                },

                //Add hash link (e.g. `#foo`) to URL or not.
                updateUrl: false //defaults to `true`.
            });

            jQuery(window).on('load', function () {

                setTimeout(checkNavigation, 150);

                var scrollTimer;

                $(document).on('scroll', function () {
                    if (typeof s === "undefined")
                        return;

                    clearTimeout(scrollTimer);
                    scrollTimer = setTimeout(checkNavigation, 100);
                });
            });
        }
    }, 50);
});